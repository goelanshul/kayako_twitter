package com.kayako.twitterapi.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kayako.twitterapi.utils.HttpRequestProcessor;

/**
 * 
 * @version 1.0, Aug 10, 2016
 * @author goel.anshul
 */
// Handles the request of the type /api
public class RequestHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RequestHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpRequestProcessor.getTweets(request, response);
	}

	// Since currently it's a get api, nothing done in post
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

}
