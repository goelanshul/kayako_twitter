package com.kayako.twitterapi.exceptions;

/**
 *  
 *  @version     1.0, Jul 8, 2015
 *  @author goel.anshul
 *  Custom Exception for all DB related exceptions
 */
public class DBException extends Exception{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String message;
    
    
    public DBException(){
        super();
    }
    
    public DBException(String message) {
        super(message);
        this.message = message;
    }
 
    public DBException(Throwable cause) {
        super(cause);
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }
    

}
