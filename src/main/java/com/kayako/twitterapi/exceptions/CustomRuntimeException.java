package com.kayako.twitterapi.exceptions;

/**
 *  
 *  @version     1.0, Aug 10, 2016
 *  @author goel.anshul
 */
public class CustomRuntimeException extends RuntimeException{
    
    /**
     * 
     */
    private static final long serialVersionUID = 4677887541592220396L;
    String message;
    
    public CustomRuntimeException (){
        super();
    }
    
    public CustomRuntimeException(String message) {
        super(message);
        this.message = message;
    }
 
    public CustomRuntimeException(Throwable cause) {
        super(cause);
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }

}
