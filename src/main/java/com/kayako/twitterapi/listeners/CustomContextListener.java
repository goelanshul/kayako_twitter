package com.kayako.twitterapi.listeners;

import java.net.UnknownHostException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.kayako.twitterapi.common.Configuration;
import com.kayako.twitterapi.common.Constants;
import com.kayako.twitterapi.db.MongoConnector;
import com.kayako.twitterapi.exceptions.CustomRuntimeException;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * @version 1.0, Aug 10, 2016
 * @author goel.anshul
 */
@WebListener
public class CustomContextListener implements ServletContextListener {

	private MongoConnector getMongoconnector(MongoClient client) throws UnknownHostException {
		MongoConnector connector = MongoConnector.getInstance(client);
		return connector;

	}

	private MongoClient getMongoClient(String host, int port) throws UnknownHostException {
		ServerAddress address = new ServerAddress(host, port);
		MongoClient mongoClient = new MongoClient(address);
		return mongoClient;
	}

	private Twitter getTwitterClient(String consumerKey, String consumerSecret, String accessToken,
			String accessTokenSecret) {

		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setOAuthConsumerKey(consumerKey);
		cb.setOAuthConsumerSecret(consumerSecret);
		cb.setOAuthAccessToken(accessToken);
		cb.setOAuthAccessTokenSecret(accessTokenSecret);
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();

		return twitter;

	}

	// Need to find a more generic way to destroy context of DB client
	public void contextDestroyed(ServletContextEvent sce) {
		MongoClient mongoClient = null;
		try {
			mongoClient = (MongoClient) sce.getServletContext().getAttribute("DB_CLIENT");
		} catch (Exception e) {
			System.out.println("Error in obtaining DB Client ");
			e.printStackTrace();
		} finally {
			if (mongoClient != null)
				mongoClient.close();
		}
	}

	public void contextInitialized(ServletContextEvent sce) {
		try {
			Configuration applicationConfig = Configuration.getInstance();
			String host = applicationConfig.getStringProperty(Constants.DB_HOST, "127.0.0.1");
			int port = applicationConfig.getIntProperty(Constants.DB_PORT, 27017);
			String consumerKey = applicationConfig.getStringProperty("twitter.consumer.key",
					"ZOvh1RHMwpMwq8fhHEDz0zC4r");
			String consumerSecret = applicationConfig.getStringProperty("twitter.consumer.secret",
					"hq461L1QmHWxWbpwZZz7tlPdKcvxQlG0fdFDFhOACHW9S1b8W1");
			String accessToken = applicationConfig.getStringProperty("twitter.access.token",
					"127161035-1lBjNlQt7yIOZxBCEmPHz3IgidnGs75VFFyHXW5f");
			String accessTokenSecret = applicationConfig.getStringProperty("twitter.access.token.secret",
					"TTd0VLrbDh7uDY3OmPWmonNQiGmgzEQXvgE6UqnA7R1A1");

			//MongoClient mongoClient = getMongoClient(host, port);
			Twitter twitterInstance = getTwitterClient(consumerKey, consumerSecret, accessToken, accessTokenSecret);
			// Uncomment following lines if DB related operations are performed for any request
			//sce.getServletContext().setAttribute("DB_CLIENT", mongoClient);
			//sce.getServletContext().setAttribute("DB_CONNECTOR", getMongoconnector(mongoClient));
			sce.getServletContext().setAttribute("TWITTER_CLIENT", twitterInstance);

		} catch (Exception e) {
			throw new CustomRuntimeException("DB Connector initialization failed");
		}
	}

}
