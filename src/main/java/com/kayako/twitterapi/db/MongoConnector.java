package com.kayako.twitterapi.db;

import java.util.ArrayList;
import java.util.List;

import com.kayako.twitterapi.common.Configuration;
import com.kayako.twitterapi.common.Constants;
import com.kayako.twitterapi.dto.DatabaseDTO;
import com.kayako.twitterapi.exceptions.DBException;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;

/**
 * @version 1.0, Aug 10, 2016
 * @author goel.anshul
 */
public class MongoConnector implements DBConnector {
	private static final String DB_NAME;
	private static final String COLLECTION_NAME;

	static {
		DB_NAME = Configuration.getInstance().getStringProperty(Constants.DB_NAME, "kayako_twitter");
		COLLECTION_NAME = Configuration.getInstance().getStringProperty(Constants.MONGODB_COLLECTION_NAME,
				"twitter_docs");
	}
	private static MongoConnector connector;
	private DBCollection collection;

	public MongoConnector(MongoClient client) {
		collection = (DBCollection) client.getDB(DB_NAME).getCollection(COLLECTION_NAME);
	}

	public static MongoConnector getInstance(MongoClient client) {
		if (null == connector) {
			synchronized (MongoConnector.class) {
				if (null == connector) {
					connector = new MongoConnector(client);
				}
			}
		}
		return connector;
	}

	public boolean insertData(DatabaseDTO databaseDTO) throws DBException {
		DBObject document = databaseDTO.toDBObject();
		try {
			return collection.insert(document).getLastError().ok();
		} catch (Exception e) {
			throw new DBException("Unable to Insert document : " + databaseDTO.getId());
		}
	}

	public String getStringValueFromDB(String searchKey, String id, String key) throws DBException {
		String value = "";
		BasicDBObject Query = new BasicDBObject();
		Query.append(searchKey, id);
		try {
			DBCursor cursor = collection.find(Query);
			if (cursor.hasNext()) {
				value = cursor.next().get(key).toString();
			}
			return value;
		} catch (Exception e) {
			throw new DBException("There was an error fetching " + key + " from DB for id : " + id);
		}

	}

	public int getIntValueFromDB(String searchKey, String id, String key) throws DBException {
		int value = 0;
		BasicDBObject Query = new BasicDBObject();
		Query.append(searchKey, id);
		try {
			DBCursor cursor = collection.find(Query);
			if (cursor.hasNext()) {
				value = Integer.parseInt(cursor.next().get(key).toString());
			}
			return value;
		} catch (Exception e) {
			throw new DBException("There was an error fetching " + key + " from DB for id : " + id);
		}

	}

	public List<DatabaseDTO> getDocuments() throws Exception {
		List<DatabaseDTO> result = new ArrayList<DatabaseDTO>();
		try {
			DBCursor cursor = collection.find(new BasicDBObject());
			while (cursor.hasNext()) {
				DBObject doc = cursor.next();
				DatabaseDTO userDoc = new DatabaseDTO().fromDBObject(doc);
				result.add(userDoc);
			}
			return result;
		} catch (Exception e) {
			throw new DBException("There was an exception while fetching documents from DB");
		}
	}

	public boolean updateData(String updateKey, String id, String fieldName, String text) throws DBException {

		BasicDBObject updateQuery = new BasicDBObject();
		updateQuery.append("$set", new BasicDBObject().append(fieldName, text));

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.append(updateKey, id);

		WriteResult result = null;
		try {
			result = collection.update(searchQuery, updateQuery);
			return result.getLastError().ok();

		} catch (Exception e) {
			throw new DBException("There was an error while updating data in DB for id : " + id);
		}

	}

	public boolean isExisting(String key, String id) throws DBException {
		BasicDBObject query = new BasicDBObject();
		query.put(key, id);
		ArrayList<String> values = new ArrayList<String>();
		values.add(id);
		query.append(key, new BasicDBObject("$in", values));
		try {
			DBCursor result = collection.find(query);
			if (result.size() > 0)
				return true;
			else
				return false;
		} catch (Exception e) {
			throw new DBException("Error while checking for " + key + " in DB");
		}

	}

	public boolean deleteData(String searchKey, String id) throws DBException {
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put(searchKey, id);
		try {
			DBCursor cursor = collection.find(searchQuery);
			if (cursor != null) {
				collection.remove(searchQuery);
			}
			return true;
		} catch (Exception e) {
			throw new DBException("There was an exception while deleting from DB for " + searchKey + " : " + id);
		}
	}

}
