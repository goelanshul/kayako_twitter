package com.kayako.twitterapi.db;
import com.kayako.twitterapi.dto.DatabaseDTO;
import com.kayako.twitterapi.exceptions.DBException;

/**
 *  
 *  @version     1.0, Aug 10, 2016
 *  @author goel.anshul
 *  Interface which should be extended for creating any DB connector
 */
public interface DBConnector {
    
    public boolean insertData(DatabaseDTO databaseDTO) throws DBException;
    
    public String getStringValueFromDB(String searchKey,String id, String key) throws DBException;
    
    public int getIntValueFromDB(String searchKey,String id, String key) throws DBException;
    
    public boolean updateData(String updateKey,String id, String fieldName,String text) throws DBException;
    
    public boolean deleteData(String searchKey,String id) throws DBException;
    
    public boolean isExisting(String key,String value) throws DBException;
    
}
