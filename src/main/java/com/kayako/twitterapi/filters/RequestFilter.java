package com.kayako.twitterapi.filters;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.kayako.twitterapi.common.Constants;

/**
 * @version 1.0, Aug 10, 2016
 * @author goel.anshul
 */

// Custom filter which used to put necessary parameters for request before they
// are resolved
@WebFilter(asyncSupported = true)
public class RequestFilter implements Filter {
	public RequestFilter() {

	}

	public void init(FilterConfig arg0) throws ServletException {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		chain.doFilter(request, response);
	}

	public void destroy() {
		// TODO Auto-generated method stub

	}

}
