package com.kayako.twitterapi.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.kayako.twitterapi.common.Constants;
import com.kayako.twitterapi.common.CustomString;
import com.kayako.twitterapi.db.DBConnector;
import com.kayako.twitterapi.dto.DatabaseDTO;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;

/**
 * @version 1.0, Aug 10, 2016
 * @author goel.anshul
 */
public class HttpRequestProcessor {

	/**
	 * Fetches Tweet data and return in Json format
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static void getTweets(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String keyWord = request.getParameter(Constants.KEY_WORD);
		String tweetCount = request.getParameter(Constants.MAX_RESULT_COUNT);
		String strRetweetCount = request.getParameter(Constants.RETWEET_COUNT);
		int retweetCount = 1;

		if (strRetweetCount != null)
			retweetCount = Integer.parseInt(strRetweetCount);

		Twitter twitterClient = (Twitter) request.getServletContext().getAttribute("TWITTER_CLIENT");
		Query query = new Query();

		

		try {
			if (keyWord != null)
				query.setQuery(keyWord);
			else
				query.setQuery("#custserv");
			if (tweetCount != null)
				query.setCount(Integer.parseInt(tweetCount));
			JsonObject responseJson = new JsonObject();
			JsonArray responseArray = new JsonArray();
			QueryResult result = twitterClient.search(query);
			for (Status status : result.getTweets()) {
				if (status.getRetweetCount() >= retweetCount)
					responseArray.add(convertStatusToJson(status));
			}
			if (responseArray.size() > 0) {
				responseJson.addProperty("resultCount", responseArray.size());
				responseJson.add("tweets", responseArray);
				responseJson.addProperty("message", "Success");

			} else {
				responseJson.addProperty("resultCount", 0);
				responseJson.addProperty("message", "No Tweets were found as per the requirements!!");
				responseJson.add("tweets", responseArray);

			}
			response.setContentType("application/json");
			response.getWriter().write(responseJson.toString());

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(500);
			response.setContentType("text/plain");
			response.getWriter().write("There was an error fetching the tweets!! Please try again later");
		}

	}

	private static JsonObject convertStatusToJson(Status status) {
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("createTime", status.getCreatedAt().toString());
		jsonObject.addProperty("tweetId", status.getId());
		jsonObject.addProperty("language", status.getLang());
		jsonObject.addProperty("tweet", status.getText());
		jsonObject.addProperty("retweetCount", status.getRetweetCount());
		jsonObject.addProperty("userId", status.getUser().getId());
		jsonObject.addProperty("twitterHandle", status.getUser().getScreenName());
		jsonObject.addProperty("favoriteCount", status.getFavoriteCount());
		return jsonObject;
	}

}
