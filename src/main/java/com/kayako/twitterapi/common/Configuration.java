package com.kayako.twitterapi.common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * 
 * @version 1.0, Aug 10, 2016
 * @author goel.anshul
 */

public class Configuration {

	private static Configuration applicationConfig = new Configuration();

	Properties properties = null;

	private Configuration() {
		properties = new Properties();
		InputStream input = null;
		OutputStream out = null;
		try {
			input = new FileInputStream(
					System.getProperty("catalina.home") + "/webapps/ROOT/WEB-INF/classes/kayako_twitter.properties");

			properties.load(input);
			properties.load(input);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Configuration getInstance() {
		return applicationConfig;
	}

	public Properties getProperties() {
		return properties;
	}

	public String getStringProperty(String key, String defaultValue) {
		if (properties.getProperty(key) == null || properties.getProperty(key).isEmpty()) {
			return defaultValue;
		}
		return properties.getProperty(key);
	}

	public int getIntProperty(String key, int defaultValue) {
		if (properties.getProperty(key) == null)
			return defaultValue;
		return Integer.parseInt(properties.getProperty(key));
	}

	public static void main(String args[]) {
		Configuration config = Configuration.getInstance();
		System.out.println(config.getProperties());
	}

}
