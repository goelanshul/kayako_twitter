package com.kayako.twitterapi.common;

import java.util.ArrayList;

/**
 * @version 1.0, Jul 4, 2015
 * @author goel.anshul
 */

// This is a custom DS used for easily manipulating and merging data from User and DB
public class CustomString {

    String       text;
    CustomString previousData;
    CustomString nextData;
    int          length;

    public CustomString(String text) {
        this.text = text;
        this.length = text.length();
    }

    public CustomString(CustomString previousData, CustomString newData) {
        this.previousData = previousData;
        this.nextData = newData;
        this.length = previousData.length();
    }

    public int length() {
        return this.length;
    }

    public CustomString concat(CustomString cs1, CustomString cs2) {
        if (null == cs1) {
            return cs2;
        } else if (null == cs2) {
            return cs1;
        } else {
            return new CustomString(cs1, cs2);
        }
    }

    public CustomString concat(CustomString cs) {
        return concat(this, cs);
    }

    public char charAt(long i) {
        assert i >= 0 && i < length;
        if (previousData == null) {
            return text.charAt((int) i);
        } else if (previousData.length() > i) {
            return previousData.charAt(i);
        } else {
            return nextData.charAt(i - previousData.length);
        }
    }

    public ArrayList<CustomString> splitInTwo(CustomString cs, int index) {

        ArrayList<CustomString> csList = new ArrayList<CustomString>();
        CustomString oldData, newData;
        if (cs.previousData == null) {
            assert index >= 0 && index <= cs.length();
            if (index == 0) { // splitting at begining gives one null node
                oldData = null;
                newData = cs;
            } else if (index == cs.length) { // splitting at end point results in newnode as null
                oldData = cs;
                newData = null;
            } else {
                oldData = new CustomString(cs.text.substring(0, index));
                newData = new CustomString(cs.text.substring(index, cs.text.length()));
            }

        } else if (cs.length == index) { // If index is equal to left node length, then simply return nodes os given object 
            oldData = cs.previousData;
            newData = cs.nextData;
        } else if (index < cs.length) { // If index is lt than left node length, split left as per index, return part 1 of it,and concat remaining with nextdata
            ArrayList<CustomString> csList1 = splitInTwo(cs.previousData, index);
            oldData = csList.get(0);
            newData = csList.get(1).concat(cs.nextData);
        } else { // Similar logic as above except split right here
            ArrayList<CustomString> csList1 = splitInTwo(cs.previousData, index - cs.length);
            oldData = cs.previousData.concat(csList1.get(0));
            newData = csList.get(1);
        }
        csList.add(oldData);
        csList.add(newData);
        return csList;

    }

    public ArrayList<CustomString> splitInTwo(int index) {
        return splitInTwo(this, index);
    }

    public CustomString insert(CustomString cs, int index) {
        ArrayList<CustomString> csList = this.splitInTwo(index);
        return concat(csList.get(0), cs);
    }

    public CustomString remove(int start, int end) {
        ArrayList<CustomString> csList = splitInTwo(this, start);
        ArrayList<CustomString> csList2 = splitInTwo(csList.get(1), end - start + 1);
        return concat(csList.get(0), csList2.get(1));
    }

    public String toString() {
        if (previousData == null)
            return text;
        else
            return previousData.toString() + nextData.toString();
    }

}
