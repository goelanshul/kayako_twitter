package com.kayako.twitterapi.common;

/**
 * @version 1.0, Jul 4, 2015
 * @author goel.anshul
 */
public class Constants {

    public static final String DB_HOST                 = "db.host";
    public static final String DB_PORT                 = "db.port";
    public static final String DB_NAME                 = "db.name";
    public static final String PRIMARY_KEY             = "id";
    public static final String REQUEST_ID              = "id";
    public static final String REQUEST_DATA            = "text";
    public static final String DATA_FIELD              = "text";
    public static final String MONGODB_COLLECTION_NAME = "mongodb.collection.name";
    public static final String HASH_TAG				   = "hashTag";
    public static final String KEY_WORD				   = "keyWord";
    public static final String MAX_RESULT_COUNT    	   = "maxCount";
    public static final String RETWEET_COUNT		   = "retweetCount";

}
