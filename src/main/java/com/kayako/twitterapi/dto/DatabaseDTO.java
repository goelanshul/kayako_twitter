package com.kayako.twitterapi.dto;

import com.kayako.twitterapi.common.Constants;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * This can be enhanced if any response has to be saved in DB
 * 
 * @version 1.0, Jul 4, 2015
 * @author goel.anshul
 */
public class DatabaseDTO {
	private long timestamp;
	private String id;
	private String text;

	private String creator;

	public DatabaseDTO() {
		timestamp = 0;
		id = "";
		text = "";
		creator = "";
	}

	public DatabaseDTO(long time, String id, String data, int users, String creator) {
		timestamp = time;
		this.id = id;
		this.creator = creator;
		text = data;
	}

	public DatabaseDTO(String id, String data, int users, String creator) {
		this.id = id;
		this.creator = creator;
		text = data;
	}

	// Setters for the DTO
	public void setTime(long time) {
		timestamp = time;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setText(String data) {
		text = data;
	}

	public void setCreator(String username) {
		creator = username;
	}

	// Getters for the DTO
	public long getTimeStamp() {
		return timestamp;
	}

	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public String getCreator() {
		return creator;
	}

	// Converting current DTO to Mongo Object
	public DBObject toDBObject() {
		DBObject mongoDocument = new BasicDBObject();
		mongoDocument.put("timestamp", timestamp);
		mongoDocument.put(Constants.PRIMARY_KEY, id);
		mongoDocument.put(Constants.DATA_FIELD, text);
		mongoDocument.put("creator", creator);
		return mongoDocument;
	}

	// Obtaining a Mongo Object from given DTO
	public DBObject toDBObject(DatabaseDTO databaseDTO) {
		DBObject mongoDocument = new BasicDBObject();
		mongoDocument.put("timestamp", databaseDTO.getTimeStamp());
		mongoDocument.put(Constants.PRIMARY_KEY, databaseDTO.getId());
		mongoDocument.put(Constants.DATA_FIELD, databaseDTO.getText());
		mongoDocument.put("creator", databaseDTO.getCreator());
		return mongoDocument;
	}

	// Getting MongoDocDTO from MongoObject
	public DatabaseDTO fromDBObject(DBObject mongoDocument) {
		if (mongoDocument.get("timestamp") != null) {
			timestamp = (Long) mongoDocument.get("timestamp");
		}
		if (mongoDocument.get(Constants.PRIMARY_KEY) != null) {
			id = (String) mongoDocument.get(Constants.PRIMARY_KEY);
		}
		if (mongoDocument.get(Constants.DATA_FIELD) != null) {
			text = (String) mongoDocument.get(Constants.DATA_FIELD);
		}

		if (mongoDocument.get("creator") != null) {
			creator = (String) mongoDocument.get("creator");
		}
		return this;
	}

}
