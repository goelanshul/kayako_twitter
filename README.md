# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is a public repository. It is an attempt to build a Twitter api client which returns with tweets as per the parameters passed to it.
Version : 1

### How do I get set up? ###

For running the code :

- Install Maven
- Install Java
- Install Tomcat (7 or above)
- Once all dependencies are installed, just build war by running mvn package or mvn clean install in directory where pom is there.
- Copy the ROOT.war from target directory to tomcat/webapps and start tomcat.
- Get json response by hitting the url /api.




For any further queries, you can contact :
Anshul Goel : 2007.anshul@gmail.com